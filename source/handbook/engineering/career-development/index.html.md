---
layout: markdown_page
title: "Engineering Career Development"
---

## Engineering Organization Roles at GitLab

Most important is the fork between purely technical work and managing teams. It's important that Engineering Organization Managers self-select into that track and don't feel pressured. We believe that management is a craft like any other and requires dedication. We also believe that everyone deserves a manager that is passionate about their craft.

Once someone reaches a Senior-level role, and wants to progress, they will need to decide whether they want to remain purely technical or pursue managing technical teams. Their manager can provide opportunities to try tasks from both tracks if they wish. Staff-level roles and Engineering Manager roles are equivalent in terms of base compensation and prestige.

## Development Roles

<table style="text-align:center;">
  <tr>
    <td colspan="4" style="border:0;">&nbsp;</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/developer#staff-developer">Staff Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/developer#distinguished-developer">Distinguished Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/developer#engineering-fellow">Engineering Fellow</a></td>
  </tr>
  <tr>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/developer#developer">Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/developer#senior-developer">Senior Developer</a></td>
    <td style="border:0; vertical-align:middle;">↗</td>
  </tr>
  <tr>
    <td style="border:0; vertical-align:middle;">↘</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/engineering-management#engineering-manager">Engineering Manager</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/engineering-management#director-of-engineering">Director of Engineering</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/engineering-management#vp-of-engineering">VP of Engineering</a></td>
  </tr>
  <tr>
    <td colspan="4" style="border:0;">&nbsp;</td>
  </tr>
</table>

Note that we have a specific section for Senior Developer because it's an important step in the technical development for every engineer. But "Senior" can optionally be applied to any role here indicating superior performance. However, it's not required to pass through "senior" for roles other than Developer. Also note that we occasionally hire Junior Developers if they have demonstrated both aptitude and our values and we believe they will quickly develop into a Developer.

## Security Engineering Roles

<table style="text-align:center;">
  <tr>
    <td colspan="4" style="border:0;">&nbsp;</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/security#staff-security-engineer">Staff Security Engineer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/security#distinguished-security-engineer">Distinguished Security Engineer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/security#engineering-fellow-security">Engineering Fellow - Security</a></td>
  </tr>
  <tr>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/security#associate-security-engineer">Associate Security Engineer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/security#senior-security-engineer">Senior Security Engineer</a></td>
    <td style="border:0; vertical-align:middle;">↗</td>
  </tr>
  <tr>
    <td style="border:0; vertical-align:middle;">↘</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/security-management#security-engineering-manager">Security Engineering Manager</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle;"><a href="/jobs/security-management#director-of-security">Director of Security</a></td>
  </tr>
  <tr>
    <td colspan="4" style="border:0;">&nbsp;</td>
  </tr>
</table>

