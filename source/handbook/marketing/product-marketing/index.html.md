---
layout: markdown_page
title: "Product Marketing"
---
# Acronyms 
**PM** - Product Managment or Product Manger. The product team as a whole, or the specific person responsible for a product area.  
**PMM** - Product Marketing Managment or Product Marketing Manager. The product marketing team as a whole, or the specific person responsible for a product marketing area.

# Release vs Launch  
A [product release, and a marketing launch are two separate activties](http://www.startuplessonslearned.com/2009/03/dont-launch.html). The canonical example of this is Apple. They launch the iPhone at their yearly event and then releases it months later. At GitLab we do it the other way: Release features as soon as they are ready letting customers use them right away, and then, do a marketing launch later when we have market validation. 

| Release | Launch | 
|-|-|
| PM Led | PMM Led | 
| New features can ship with or without marketing support | Launch timing need not be tied to the proximity of when a feature was released | 


### Customer Case Study Creation<a name="case-study"></a>

#### **Objectives**:
- Illustrate successful and measurable improvements in the customer’s business   
- Show other users how customers are using GitLab
- Align content so that GitLab solutions reflect the challenges our customers, prospects, and the market requirements
- Develop and grow organisational relationships: speak at events on our behalf, promote their business
- Support our value proposition - Faster from idea to production

#### **Creating the Customer Case Study**:

**Explaining the Case Study process to a Customer & Creating the Case Study**:

Below is an example email template that you can send customers to after the AE has introduced you to the customer. The email below will help the customer to get a better sense of what we are asking of them.

>Hi X,

>It's nice to e-meet you, we'd love to hear more about your journey to GitLab and potentially write a customer story on COMPANY NAME.

>Here are the steps that we'd work with you on.
>- 20-30 minute phone call to hear more about your industry, business, and team. (In the call, we would also like to hear more about your decision making process to first choose GitLab and then eventually purchase EE.)
>- Review of the draft customer story
>- Final review of the customer story Then once the customer story is agreed upon by you or someone on your team we will publish it on GitLab's channels.

>Please let me know if you're open to kicking off the customer story process on X date

#### **Collecting Metrics**:
Possible quantitative metrics that the customer can be asked to share with GitLab include:
- Reduced cycle time
- Number of deploys in a given time frame
- Reduced number of bugs or Reverts
- Recuced number of admin hours
- Cost savings % through purchasing GitLab: Reduce the cost of managing a number of different people, projects, and platforms.
- Reduction in internal support tickets requests: Reduction in the number of support tickets team submitting to fix challenges, compared to initial SCM tool

The customer case study should then be written by Product Marketing team, and the draft sent to the customer for their input, and approval.

Other sections in the case study can be found on the customer's website or by searching the web - Sidebar summary, The Customer.

Following approval from the customer, the Design team should be sent a doc of the case study to include in the case study design template. The case study can then be published on our website.

#### **Case Study Format**:

*Headline:* The name of the customer and the benefit they gained from implementing our solution

*Sidebar Summary:* Summarize key points
- Customer Name and Logo
- Customer Details: Country, Website
- Organisation Type - Public/Private & Industry
- Annual Revenue - If publicly available
- Employees
- Summary of Key Benefits

*The Customer:* A brief introduction of the customer, not the solution.

*The Challenge:* What was the customer trying to change or improve. What regulations or market conditions drove the customer to search for a new solution. Share the steps the customer took to solve the problem, including other products and services they investigated.

*The Solution:* Detail the solution and how it aligns to the customers requirements. Also detail other solutions that GitLab interfaces with. Also include customer quote.

*The Results:* Detail how GitLab supported the customer to solve their problems. This is where the use of benchmarking metrics such as such as time saved, reduced costs, increased performance etc. are required.

*About GitLab:* Short paragraph on GitlLab - about, solutions etc. Call to action of solutions offered.

*Possible Additional Supporting Documents:*
- Customer Deal Summary – High Level PowerPoint summary after deal first signed.
- Customer Success Overview
- Infographic – Single page A4 summary with diagrams and measureable benchmarks
- Benchmark Metrics
- Publish on website
- Video - Short video summary if customer is willing to participate - Perforce example
- Blog Post - Blog post to launch customer case study
- Keywords for SEO etc.

#### **Creating Case Study Blog Post**:
Refer to the [guide available here](/handbook/marketing/marketing-sales-development/content/#casestudyblogposts).
