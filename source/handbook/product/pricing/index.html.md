---
layout: markdown_page
title: Pricing
---

## Departments

Pricing affects product, marketing, and sales.
Therefore general pricing decisions are made by the CEO.

Product makes most decisions on a day to day basis about what feature should go in what plan based on [the Enterprise Edition Tiers
](https://about.gitlab.com/handbook/product/#enterprise-edition-tiers).
Therefore this pricing page is in the product handbook.

## Four tiers

We have four pricing tiers.
How we make decisions on a day to day basis are specified in [what goes in what version](https://about.gitlab.com/handbook/product/#what-goes-in-what-version).

| Version: | CE | EES | EEP | EEU |
| Per user per year | $0 | $39 | $199 | $999 |
| [Market segment](https://about.gitlab.com/handbook/sales/#market-segmentation): | SMB | Mid Market | Large | Strategic |
| Buyer: | None | Developers | IT | CIO |
| Main competitor: | None | Atlassian | GitHub | Collabnet |

## Hybrid sales model

There is a big price difference between the different plans (0$, $39, $199, $999 per user per year, a price difference of infinite, 5x, 5x, 5x). For GitLab Inc. the majority of revenue is coming from the large enterprises buying the top two tiers.

Most companies in a similar situation would focus only on the highest tiers. But we want to make a our hybrid model work for the following reasons:

1. We want to keep being a [good steward of the open source project](https://about.gitlab.com/stewardship/).
1. The lower two tiers are a scalable way to create future customers.
1. If organization are already using Atlassian JIRA we want to be competitive with BitBucket on pricing. If they buy BitBucket it is hard for us to win them back. If they buy GitLab they discover that it can replace JIRA and over time they might buy a higher tier.

## Selling per main feature

We tried selling one feature at a time but this was was not feasible.
An improved version of that would be selling 7 main features instead of 3 plans.
Examples of main features would be High Availability, Security, Service Desk, etc.

The advantages are:

1. Gradual upgrading to more expensive features.
1. Pay only for the features you use.
1. Add-ons are a common way of selling this.

The disadvantages are:

1. It is [suboptimal for both the buyer and GitLab Inc.](http://cdixon.org/2012/07/08/how-bundling-benefits-sellers-and-buyers/).
1. It is hard for the buyer to estimate how much of each feature they will need.
1. The complexity lengthens the sales process.
1. For users it is unclear what features they can use.
1. The true-up process becomes more complex.
1. The customer has to administer a process how users can get more features.
1. Features get less usage and therefore the improvements are slower.
1. It is hard to do with a hybrid sales model where there is a 25x difference between the lowest and highest paid plan.

## Multiple plans for one customer

We considered selling multiple plans to the same customer, allowing them some users on every plan.

The advantages are:

1. Gradual upgrading to more expensive features per team.
1. Pay only for the features you use.

The disadvantages are:

1. It is hard for the buyer to estimate how much of each tier they will need.
1. The complexity lengthens the sales process.
1. For users it is unclear what features they can use.
1. It is not common in the industry, buyers don't expect it, and it isn't a boring solution (a sub-value under our [efficiency value](https://about.gitlab.com/handbook/values/#efficiency)).
1. The true-up process becomes more complex.
1. Some features are can't be disabled on a per user basis, like High Availability (HA).
1. The customer has to administer a process how users can get a higher plan.

We currently think the disadvantages outweigh the advantages
