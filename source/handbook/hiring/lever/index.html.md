---
layout: markdown_page
title: "Lever"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## How to join Lever

[Lever](hire.lever.co/) is GitLab's ATS (Applicant Tracking System). All hiring managers and interviewers will use Lever to review resumes, provide feedback, communicate with candidates, and more.

When you [log in to Lever](https://hire.lever.co/auth/login) for the first time, choose "Sign in with Google", making sure you're already logged in to your GitLab email account. All GitLab team members can log in to [Lever](hire.lever.co) and be granted the “Interviewer” role, the most basic [access role](https://help.lever.co/hc/en-us/articles/205644649). Until their access is upgraded by an admin, they will only have access to one page in Lever, [Interviews](https://hire.lever.co/interviews), where employees can complete interview feedback and make referrals. If you need your access to be upgraded so you can review candidate profiles, please reach out to People Ops.

You can read more about [how to log in to Lever here](https://help.lever.co/hc/en-us/articles/204485965-How-do-I-log-in-). Please note that Lever only officially supports Chrome on desktop. While they are actively working on optimizing Lever across browsers, they recommend using Lever on Chrome.

## How to set up your Lever account

After joining Lever, it is important for all interviewers and hiring managers to connect their Google and Slack accounts to Lever to optimize their experience and the use of the platform.

### Sync Google account

Please read this article on [how to sync your Google account to Lever](https://help.lever.co/hc/en-us/articles/204502445-How-does-Lever-s-Gmail-sync-work-).
Syncing your Google account (both your email and your calendar) allows for seamless communication with candidates and coworkers, as well as a [Lever scheduling feature called Easy Book](https://help.lever.co/hc/en-us/articles/115003800823-How-do-I-schedule-an-interview-using-Lever-s-Easy-Book-feature-), where each candidate will be given a unique link to your calendar in order to schedule an interview.


#### Scheduling interviews with Easy Book

The recruiting team will schedule all interviews and send out unique Easy Book links, along with a link to a Zoom room for ALL interviewers, which will follow this format: `https://gitlab.zoom.us/my/gitlab.firstnamelastname`  (e.g. Joe Smith's would be: `https://gitlab.zoom.us/my/gitlab.joesmith`). This room will be your consistent location for interviews, but it will not show up in your calendar invite. We recommend you bookmark this link for easy access. All interviews will be conducted via Zoom to create a streamlined hiring process. Recruiting is working with Lever to be able to automatically add links to calendar invites. In the interim, the Recruiting team will manually add Zoom links to the interview invites; if your event is missed, please remember that all interview link will still be on Zoom and follow the convention above.

#### Important Notes
* Please be sure to go to your [Google Calendar settings](https://calendar.google.com/calendar/r/settings?tab=mc) and input your current time zone.
* Easy Book will only show candidates times you are available within the hours of 9am-5pm your local time (per the above point). If you would like to only have interviews during a specific period of your day, please block off the rest of your day as unavailable so Easy Book will not offer those times. You will need to have the status of the event marked as "busy" in order for this to work; typically timed events default to "busy" and all-day events default to "free", so please be aware of the settings when creating calendar events. Reach out to People Ops if you have questions on this.
* When a candidate clicks on the Easy Book link, it will show them the timezone of whoever sent them the link. They will need to change it to their local timezone to see the correct times. Candidates can do this by clicking on the timezone field at the top of the screen.
* To insure that someone else (another candidate, for example) does not join your video call without your permission, log into your Zoom account online. Click on "My Account" in the top right corner, and click on "Meeting Settings" in the left sidebar. Scroll down to "In Meeting (Advanced)", and toggle on the option "Waiting Room".

### Sync Slack account

Please read this article on [how to sync your Slack account to Lever](https://help.lever.co/hc/en-us/articles/115000574666). The Slack integration will send you updates about upcoming interviews ten minutes before the interview starts. It will include the candidate information as well as a link to view their resume/profile and to leave feedback.

## How to view and follow candidates in Lever

When you log in to Lever, you will be brought to the [Candidates page](https://hire.lever.co/candidates). At the top left corner, you can filter which candidates you are looking at. It's recommended for interviewers and hiring managers to select "Followed Candidates". Read more about [the difference between the All owners, Owned by me, and Followed filters](https://help.lever.co/hc/en-us/articles/208223936-What-is-the-difference-between-the-All-owners-Owned-by-me-and-Followed-filters-).

It is helpful to understand [what "following" is](https://help.lever.co/hc/en-us/articles/203824939-What-is-following-) in order to best make use of the function. If you would like to follow a candidate or role that you do not currently have access to, please reach out to People Ops.

You will be added as a follower to a candidate whenever you follow the role they applied for, or when someone else tags you in a candidate's profile. To unfollow a specific candidate, go to their candidate profile. Beneath their name is the word "Following"; click the word and it will unfollow you. To unfollow an entire role, please reach out to People Ops.

If you are on the go and would like to access Lever via your mobile device, please read about [how to view a candidate on Mobile](https://help.lever.co/hc/en-us/articles/218355523-How-can-I-view-candidate-profiles-on-a-mobile-device-).

## How to leave feedback in Lever

After you've integrated Slack with Lever, you'll receive a Slack message reminding you of your upcoming interview, with a link to the candidate's resume. When you click that link, it will show you the details for that particular interview. Please click "Feedback Form" under their name, and track your feedback in the text box(es) during your call. If you do not submit your feedback within an hour after your interview, Slack will send you a follow up message reminding you to leave feedback.

You can also go straight to the [Interviews page](https://hire.lever.co/interviews), where you will see all of your upcoming interviews and be able to click into each one to see the details and provide feedback, as well as keep track of your past and future interviews. All team members who have logged in to Lever have access to this page.

Alternatively, if you have access to candidate profiles, you can go directly to their profile to give feedback. Next to the "Add Note" field at the top, click on the three dots "..." and choose "Add Feedback". Then choose the "General Feedback Form" and fill out the text box(es). You will be required to provide a vote of "Inclined", "Semi-Inclined", or "Not Inclined." You will also be asked to give an overall rating on a scale of 4 (4 = Strong Hire; 3 = Hire; 2 = No; 1 = Strong No).

There is an option to make feedback forms, notes, and emails "secret," but please leave all information public, unless it refers to sensitive candidate data such as compensation. Only People Ops team members and hiring managers should be discussing compensation information with candidates, so they should be the primary users of secret notes.

## How to make a referral

You can [refer someone directly](https://hire.lever.co/referrals/new) by providing their name, email address, what position(s) you would like to refer them for, as well as other relevant details, such as a LinkedIn profile, phone number, and any notes you have. Please note it is required to include their email address.

You may also create a referral link, which you can send to prospective candidates in your network, and you will automatically be credited as their referral. To do so, go to the [Interviews page](https://hire.lever.co/interviews), click on "Social Referrals", and click "Get referral link". You can share this link to your social networks directly and/or copy the link to share elsewhere.

Please see these Lever articles on [how to make a referral](https://help.lever.co/hc/en-us/articles/204493415-How-do-I-make-a-referral-) and [social referrals](https://help.lever.co/hc/en-us/articles/205697609-What-are-social-referrals-) for more information, as well as GitLab's [referral bonus program](https://about.gitlab.com/handbook/incentives/#referral-bonuses).

## Changing Stages for a Candidate

The People Ops team are the only ones who should be [changing stages for a candidate](https://help.lever.co/hc/en-us/articles/205697629-How-do-I-track-candidates-through-the-hiring-process-). GitLab has [several stages in Lever](https://help.lever.co/hc/en-us/articles/205582549-What-do-the-pipeline-stages-mean-How-can-I-customize-my-pipeline-), outlined below, which can be customized by reaching out to GitLab's Lever Implementation Specialist.

* Lead
   * New Lead
   * Reached Out
   * Responded
* Applicant
   * New Applicant
   * Contacted
   * Assessment Sent
   * Assessment In Review
* Interview
   * Screening Call
   * First Video Interview
   * Second Video Interview
   * Third Video Interview
   * Optional Final CEO Interview
   * References
   * Offer

The "Lead" bucket is exclusively used by the Recruiting team for sourcing new applicants and communicating with them.

Within the "Applicant" bucket, "New Applicant" refers to all incoming applications; "Contacted" refers to applicants that the Recruiting team has reached out to for more information; "Assessment Sent" refers to applicants whom the Recruiting team has sent an opening questionnaire to, and "Assessment In Review" refers to applicants who have returned the questionnaire and need to be reviewed by a member of the appropriate hiring team. All members of the hiring team who review applicants can refer to the "Assessment In Review" stage to see what applications still needs review.

The "Interview" bucket keeps track of where candidates are in the process. They are moved into each new stage once they have successfully passed the prior stage; e.g. a candidate who does well in the Screening Call will be moved to First Video Interview while they are scheduling the interview and until feedback is received by the interviewer.

### Changing Jobs

If a candidate applies for a job but is a better fit for another job, or if they are added as a new lead, they will need to be added to a new job. To do this, go to the candidate profile, and under their name is a button that says "Add Job". If they are already a current or previous candidate for a role, the button will be right above those roles on the right. If this is the first job they are being added to, the button will be on the right.

## Archiving Candidates

To remove a candidate from the active pipeline, they need to be [archived](https://help.lever.co/hc/en-us/articles/204502125-How-do-I-reject-archive-a-candidate-) in Lever, which should be done only by People Ops. Any time a candidate is archived, People Ops will email them letting them know. Only People Ops should be communicating rejections.

Individual candidates can be archived by clicking "Archive" in the top right corner of their profile and choosing the appropriate reason. You can also archive candidates in bulk, for example if the position has been filled. To do so, select the checkboxes for each candidate in the appropriate stage, click "Archive" from the actions that appear at the top, and choose the appropriate reason. Note: you can only choose one reason when archiving in bulk.

You can access all archived candidates by clicking on the "Archive" tab on the top right corner on the pipeline screen.

To unarchive a candidate, click "Unarchive" at the top right of their profile, choose the appropriate job they are being unarchived for, and they will be active again.

You can also [snooze a candidate](https://help.lever.co/hc/en-us/articles/204502005-How-do-I-snooze-a-candidate-) to remove them from the active pipeline but enable you to revisit them in a few weeks or months when the timing is more appropriate. To snooze someone, click on the clock at the top of their profile, input optional notes, and select how long you would like them snoozed for. You can view, edit, or delete the snooze by clicking on the same button. At the end of the selected time period, you will receive an email notification. Only the person who snoozed the candidate will receive the notification. To view all snoozed candidates, scroll the the bottom of the left sidebar and toggle "Snoozed".

## Additional Lever information
* [Dashboard Overview](https://lever.wistia.com/medias/1pv5mqnrps)
* [Candidate Profile Overview](https://lever.wistia.com/medias/5yq8pamndg)
* [What are the keyboard shortcuts and hotkeys in Lever?](https://help.lever.co/hc/en-us/articles/205633775-What-are-the-keyboard-shortcuts-and-hotkeys-in-Lever-)
* [How does search work in Lever?](https://help.lever.co/hc/en-us/articles/206403245-How-does-search-work-in-Lever-)
* [How do I add an emoji to a note in Lever?](https://help.lever.co/hc/en-us/articles/208337403-How-do-I-add-an-emoji-to-a-note-in-Lever-)
* [What does Lever have access to on my Google account?](https://help.lever.co/hc/en-us/articles/203825829-What-does-Lever-have-access-to-on-my-Google-account-)
* [How does Lever handle viruses in candidate files?](https://help.lever.co/hc/en-us/articles/115001571243-How-does-Lever-handle-viruses-in-candidate-files-)
* [How do I change my profile picture (avatar)?](https://help.lever.co/hc/en-us/articles/203825799-How-do-I-change-my-profile-picture-avatar-)
* [What's the difference between a Published, Internal, Closed, and Draft job posting?](https://help.lever.co/hc/en-us/articles/205066609-What-s-the-difference-between-a-Published-Internal-Closed-and-Draft-job-posting-)
