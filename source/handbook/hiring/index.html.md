---
layout: markdown_page
title: "Hiring"
---

## Hiring pages

- [Interviewing](/handbook/hiring/interviewing/)
- [Vacancies](/handbook/hiring/vacancies/)
- [Lever](/handbook/hiring/lever/)
- [Principles](/handbook/hiring/principles/)

## Related to hiring

- [Benefits](/handbook/benefits/)
- [Visas](/handbook/people-operations/visas/)
- [Compensation](/handbook/people-operations/global-compensation/)
- [Jobs](/jobs/)

## On this page
{:.no_toc}

- TOC
{:toc}

## Equal Employment Opportunity

 Diversity is one of GitLab's core [values](https://about.gitlab.com/handbook/values) and
 GitLab is dedicated to providing equal employment opportunities (EEO) to all team members
 and applicants for employment without regard to race, color, religion, gender,
 national origin, age, disability, or genetics. One example of how put this into practice
 is through sponsorship of [diversity events](https://about.gitlab.com/2016/03/24/sponsorship-update/)

 GitLab complies with all applicable laws governing nondiscrimination in employment. This policy applies to all terms and conditions of employment, including recruiting, hiring, placement, promotion, termination, layoff, recall, transfer,
 leaves of absence, compensation, and training. GitLab expressly prohibits any form of workplace harassment.
 Improper interference with the ability of GitLab’s team members to perform their job duties
 may result in discipline up to and including discharge. If you have any complaints, concerns,
 or suggestions to do better please [contact People Ops](/handbook/people-operations/#reach-peopleops).

## Country Hiring Guidelines

Each country has unique rules and regulations affecting the employability of its
 citizens. These rules can be complex and time consuming to navigate. To aid
 our recruiting team we have created a quick reference guide (search for a google
 spreadsheet titled "Country Hiring Guidelines" in google drive) which defines
 from which countries GitLab can add team members. If a country is not listed
 please email legal@gitlab.com with the name of candidate, location, position,
 and desired start date. A response will be provided within 48 hours.

GitLab has established entities, payroll solutions or partnered with third parties in the following countries:

1. United States
1. The Netherlands
1. United Kingdom
1. Belgium
1. China
1. India
1. Germany
