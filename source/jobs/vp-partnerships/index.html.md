---
layout: job_page
title: "Vice President of Partnerships"
---

#Responsibilities

* Identify strategic relationships with organizations suited to stimulate GitLab enterprise business
* Develop plans for strategic partnerships and defined results which tie to revenue generation from the enterprise segment
* Work with executive team to build and maintain relationships, managing and leveraging the relationship through each stage of its lifecycle
* Evaluating full risk and benefits of existing and new partnerships
* Build out tracking and reporting of partnership relationships in order to optimize
* Work cross departmentally with internal teams to build, publish, and distribute partnership assets
* Provide thought leadership, strategic insight, and clear communication to team and company on various partnership initiatives

#Requirements
* 6-8 years of senior leadership (VP or C-level) in planning and closing partnership deals valued at over $50M with the largest tech companies.
* 8+ years of experience in the field of cloud and/or DevOps
* Proven success developing and delivering on strategic plans with leading tech companies in the Cloud and DevOps spaces.
* Repeated success in penetrating Enterprise companies, outlining plans to build opportunities for collaboration, starting from cold approaches.
* Experience migrating large open-source projects to new tools for their communities
* Demonstrated complete understanding of the company, product, and landscape to make informed long-term strategic business decisions
* A networks of high executive contacts in the cloud, DevOps and open-source spaces. Proven ability to translate those to new partnerships and business opportunities.
* Demonstrated analytical and data led decision-making.
* Based on San Francisco
