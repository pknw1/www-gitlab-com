---
layout: job_page
title: "Staff Application Security Engineer"
---

As a key member of our application security team, your core responsibility is to work with functional groups across GitLab to assess the security architecture of new products and capabilities. Examples include executing and maintaining a security review program, and working with development teams to define and evangelize security best practices.

Our thesis is that [Good Security Is Holistic](https://medium.com/@justin.schuh/stop-buying-bad-security-prescriptions-f18e4f61ba9e). We think that simulating a security culture in engineering is one of the most important things. We don't do checklist security, the goal is to keep the trust of our users by being secure, compliance is not a goal in itself. We don't think that third party products are unimportant but they are not a silver bullet to making everything secure.

The [Security Team](/handbook/engineering/security) is responsible for leading and implementing the various initiatives that relate to improving GitLab's security.

## Responsibilities

- Own vulnerability management and mitigation approaches
- Conduct threat modeling tied to security services
- Conduct application security reviews
- Implement secure architecture design
- Provide security training and outreach to internal development teams
- Develop security guidance documentation
- Assist with recruiting activities and administrative work
- Define, implement, and monitor security measures to protect GitLab.com and company assets


## Requirements

- Familiarity with common security libraries, security controls, and common security flaws that apply to Ruby on Rails applications
- Some development experience (Ruby and Ruby on Rails preferred; for GitLab debugging)
- Experience with OWASP, static/dynamic analysis, and common exploit tools and methods
- An understanding of network and web related protocols (such as, TCP/IP, UDP, IPSEC, HTTP, HTTPS, routing protocols)
- Excellent written and verbal communication skills
- Demonstrable teamwork skills and resourcefulness
- Familiarity with cloud security controls and best practices
- Passion for open source
- Linux experience (e.g. Ubuntu)
- Network security experience (Routing, firewalls, VPNs, common services and protocols)
- Collaborative team spirit with great communication skills
- You share our [values](), and work in accordance with those values.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team).

- Selected candidates will be invited to schedule a screening call with a Recruiter
- Next, candidates will be invited to schedule a 45 minute technical interview with the Security Lead
- Candidates will then be invited to schedule a 45 minute interview with our Director of Security
- Candidates will be invited to schedule a one hour interview with our VP of Engineering
- Finally, candidates may have a 50 minute interview with our CEO
- Successful candidates will subsequently be made an offer via email
