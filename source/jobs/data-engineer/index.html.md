---
layout: job_page
title: "Data Engineer"
---

## Responsibilities

* Ensure the Company’s cloud and on-premise data is centralized into a single data warehouse that can support data analysis requirements from all functional groups of the Company.
* Create a common data framework so that all company data can be analyzed in a unified manner.
* Build data pipelines from internal databases and SaaS applications (SFDC, Zuora, Marketo, Zendesk, etc.) to populate and feed our data warehouse with high quality, consistent data. 
* Drive the creation of architecture diagrams and systems documentation that can be understood and used by business users and other GitLabbers. 
* Help to develop and execute a roadmap for system expansion, evaluate existing systems and ensure future systems are aligned with the Company’s data architecture plan.
* Implement a set of processes that ensure any changes in transactional system architecture are documented and their impact on the company’s overall data integrity are considered prior to changes being made.
* Collaborate with all functions of the company to ensure data needs are addressed.
* This position reports to the Senior Director, Data & Analytics. 


## Requirements

* 2+ years hands-on experience in a data analytics or data warehousing role
* Demonstrably deep understanding of SQL and relational databases (Postgres preferred).
* Experience with a high growth company using on-premise tools and on-demand (SaaS) transactional systems
* Hands-on experience with Python, SQL, and ETL tools like Talend/Kettle, Boomi, Informatica Cloud, etc. Experience with Postgres is a plus.
* Be Passionate about data, analytics, and automation.
* Experience with open source data warehouse tools.
* Strong data modeling skills and familiarity with the Kimball methodology.
* Experience with Salesforce, Zuora, Zendesk and Marketo as data sources and consuming data from SaaS application APIs.
* Share and work in accordance with our values
* Must be able to work in alignment with Americas timezones
- Successful completion of a [background check](/handbook/people-operations/#background-checks).


## Hiring Process

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Sr. Director of Data & Analytics
* Candidates will then be invited to schedule a second interview with our CFO
* Candidates will then be invited to schedule a third interview with our Sr. Director of Demand Generation
* Finally, candidates may be asked to interview with our CEO
