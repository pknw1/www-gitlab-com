layout: job_page
title: "Recruiting Sourcer"
---

GitLab is looking for two sourcing experts with experience in finding the best candidates for a high-tech, distributed company.   How you build relationships and how you subsequently build sourcing strategies to find the best talent in the world is critical.   A big picture thinker with experience in an international, highly cross-functional organization who has strong business acumen will be most successful in the role.   However, attention to detail, and experience with market mapping and deep research is equally as critical.  One role will be based in Europe, The Middle East, or Africa. In this role, it is critical for the applicants to have experience in Sales and Marketing.  Being fluent English is required to be successful at GitLab, but additional language skills are a huge plus.  Finally, we’re looking for in-depth understanding and background in hiring amazing people in at least two for these areas: Engineering, Sales, Marketing, Product.

This role will report to the Recruiting Director.

## Responsibilities

* Source for the best passive candidates in the global market. Being willing to look wherever is necessary to find the talent we need, with a focus on our values and requirements.
* Put meaningful focus on building a diverse pipeline.
* Partner directly with in-house recruiters and business leaders globally to understand specific needs to attract and engage with top talent. Someone who is highly curious and asks great questions to build their business acumen will be especially successful in the role.
* Strategically utilize LinkedIn, online research, events, etc to engage highly passive, sought after candidates.
* Develop a strong relationship with candidates and make judicious decisions on fit for a particular role or team, in addition to thinking through fit for our unique culture.
* Map out individual markets and gather intelligence around specific talent pools, using that knowledge to identify top talent.


## Requirements

* Experience sourcing and research at all levels, preferably in a global capacity within the software industry, open source experience is a plus
* Proven success in sourcing for Sales and/or technical positions. Sales is critical for the role in EMEA.
* Demonstrated ability to effectively source passive candidates. This is a fully outbound role.
* Experience with competitive global job markets preferred
* Focused on delivering an excellent candidate experience
* Ambitious, efficient and stable under tight deadlines and competing priorities
* Remote working experience in a technology startup will be an added advantage
* Ability to build relationships with managers and colleagues across multiple disciplines and timezones
* Working knowledge using an applicant tracking systems. Lever is a plus.
* Outstanding written and verbal communication skills across all levels
* Willingness to learn and use software tools including Git and GitLab
* College / University degree in Marketing, Human Resources or related field from an accredited institution preferred
- Successful completion of a [background check](/handbook/people-operations/#background-checks).
