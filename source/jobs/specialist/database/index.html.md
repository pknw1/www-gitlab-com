---
layout: job_page
title: "Database Specialist"
---

Our database specialist is a hybrid role: part developer, part database expert. You will spend the majority of your time making application changes to improve database performance, availability, and reliability; though you will also spend time working on the database infrastructure that powers GitLab.com. Infrastructure work involves (but is not limited to) tasks such as making configuration changes, adjusting monitoring, and upgrading the database.

## Example Projects

* Rewriting the database queries and related application logic used for retrieving [subgroups](https://docs.gitlab.com/ee/user/group/subgroups/index.html#subgroups)
* Rewriting code used for importing projects from other platforms (e.g. [GitHub](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14731))
* Adding trend analysis to monitoring to better detect performance and availability changes on GitLab.com
* Analyzing tables and optimizing them by adding indexes, breaking them up into separate tables, or by removing unnecessary columns.
* Reviewing database related changes submitted by other developers
* Documenting database best practices or patterns to avoid


## Requirements

* At least 5 years of experience running PostgreSQL in large production environments
* At least 3 years of experience working with either Ruby (preferred) or Python
* At least 3 years of experience with Ruby on Rails, Django, or other Ruby and/or Python web frameworks (Flask, web2py, etc)
* Solid understanding of SQL and PL/pgSQL
* Solid understanding of the internals of PostgreSQL
* Significant experience working in a distributed production environment
* You share our [values](/handbook/#values)
* Excellent written and verbal English communication skills


## Nice-to-have's

* Experience with running pgbouncer and/or WAL-E in production.
* Experience with Go, C, and/or Rust.
* MySQL experience, since GitLab also supports MySQL.

### Hiring Process

The hiring process for this role consists of _at least_

- Assessment
- [screening call](/handbook/hiring/#screening-call)
- interview with Senior Developer
- interview with Staff Production Engineer
- interview with VP of Engineering

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
