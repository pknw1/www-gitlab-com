---
layout: job_page
title: "Engineering Management"
---

## Engineering Management Roles at GitLab

Managers in the engineering department at GitLab see the team as their product. While they are technically credible and know the details of what developers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of product commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.


### Engineering Manager

* Hire a world class team of developers to work on their team
* Help their developers grow their skills and experience
* Code reviews, architecture, bugs, and small features
* Hold regular 1:1's with all members their team
* Create a sense of psychological safety on your team
* Recommend technical and process improvements
* Exquisite written and verbal communication skills
* Author project plans for epics
* Draft quarterly OKRs
* Run agile project management process
* Train engineers to screen applicants and conduct managerial interviews
* Improve product quality, security, and performance


### Director of Engineering

The Director of Engineering role extends the [Engineering Manager](#engineering-manager) role.

* Hire a world class team of managers and developers to work on their teams
* Help their managers and developers grow their skills and experience
* Manage multiple teams and projects
* Hold regular skip-level 1:1's with all members of their team
* Create a sense of psychological safety on your _teams_
* _Drive_ technical and process improvements
* _Drive_ quarterly OKRs
* _Drive_ agile project management process
* _Own_ product quality, security, and performance
* Represent the company publicly at conferences

### VP of Engineering

The VP of Engineering role extends the [Director of Engineering](#director-of-engineering) role.

* Drive recruiting of a world class team
* Help their directors, managers, and developers grow their skills and experience
* Measure and improve the happiness of engineering
* Make sure the handbook is used and maintained in a transparent way
* _Sponsor_ technical and process improvements
* _Own_ the sense of psychological safety of the department
* _Set_ quarterly OKRs around company goals
* _Define_ the agile project management process
* Spend time with customers to understand their needs and issues
* _Be accountable for_ product quality, security, and performance
