---
layout: job_page
title: "Senior Product Manager, BizOps"
---

{: .text-center}
<br>

We're looking for a product manager to invent the future of BizOps tools;
specifically, taking our approach to Complete DevOps, and applying it to the
world of business operations. We aim to help customers answer such questions as
how can I acquire the highest customer lifetime value (LTV) at the lowest
customer acquisition cost (CAC). Think about displacing tools such as Marketo,
Insightsquared, and Gainsight using open-source and deeply-integrated tools.

This is a new area for GitLab and one of our big-bets for 2018. It'll be your
job to work out what we are going to do and how.

We work in quite a unique way at GitLab, where lots of flexibility and
independence is mixed with a high paced, pragmatic way of working. And
everything we do is in the open.

## Responsibilities

- Build an attractive roadmap together with the Head of Product, VP of Product, and CEO; based on our [vision](/direction/#vision)
- Manage new features from conception to market
- Work out feature proposals from the community and customers
- Work together with UX, Frontend, and Backend engineers
- Ensure a smooth release of changes and features together with all stakeholders
- Empower the community by writing great documentation and highlighting the product in various ways in cooperation with marketing

## You are _not_ (solely) responsible for

- Shipping in time. As a PM you are part of a team that delivers a change,
the team is responsible for shipping in time, not you.
- A team of engineers. PMs at GitLab do not manage people, they manage the
_product_. You'll be required to take the lead in decisions about the product,
but it's not your role to manage the people that build the product.
- Capacity and availability planning. You will work together with engineering
managers on schedules and planning: you prioritize, the engineering managers
determine how much can be scheduled.

## Requirements

- Strong Experience in product management
- Understanding of Git and Git workflows
- Strong understanding of business operations
- Understanding of deployment infrastructure and container technologies such as Kubernetes and Docker
- Strong technically. You understand how software is built, packaged and deployed.
- Passion for design and usability
- Highly independent and pragmatic
- You are living wherever you want
- You share our [values](/handbook/values), and work in accordance with those values.
- Bonus points: experience with GitLab
- Bonus points: experience in working with open source projects

## Relevant links

- [Product Handbook](/handbook/product)
- [Engineering Workflow](/handbook/engineering/workflow)

## Hiring Process

Applicants for this position can expect the hiring process to follow the order
below. Please keep in mind that applicants can be declined from the position at
any stage of the process. To learn more about someone who may be conducting the
interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Head of Product
* Candidates will then be invited to schedule an interview with the Senior Director, Data & Analytics
* Candidates will be invited to schedule a third interview with our VP of Product
* Candidates may be asked to meet with additional team members at the manager's discretion
* Finally, candidates will interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
