---
layout: job_page
title: "Senior Product Marketing Manager"
---

GitLab seeks to add a Senior Product Marketing Manager, having technical domain experience, as well as deep experience in enterprise software marketing, sales enablement and training.


## Responsibilities

* Become product & market expert for GitLab, able to read trends and quickly iterate on messages our marketing & sales teams can use
* Contribute to launches for new product offerings
* Produce collateral such as presentations, videos, demos, whitepapers, one pagers, case studies, battle cards, etc.
* Train and enable sales and partner teams to deeply understand the buyer value proposition and features and capabilities of the product
* Support creation of enablement plans and deliver it to sales, sales development, solutions architects, and partners
* Capture how our products are used and the value created across industries and customers for collateral creation and customer reference marketing
* Collaborate with our sales development reps, account executives and partners to successfully drive the sales of GitLab’s product offerings
* Drive integrated communications initiatives with other teams across internal and partner ecosystems


## Requirements

* At least 6 years enterprise software marketing experience, including 4 directly in product marketing
* In-depth industry experience and working knowledge using or building software products, particularly cloud software, collaboration software, or developer tools
* Highly technical to be able to independently develop demos, webinars, videos, technical collateral, ROI/value oriented assessments, etc.
* Experience designing sales collateral from scratch based on sales conversations, sales calls, product interviews, user interviews, market research, and your own experience
* Experience in building sales enablement collateral and the corresponding training to ensure that salespeople can confidently and accurately deliver GitLab's enterprise value proposition.
* Experience with sales enablement in a partner & alliances context
* Experience managing sales and marketing requirements & expectations in a fast-growing environment
* Obsessive about iteration, quality, clarity, details, & execution
* Market research and competitive intelligence experience
* Stay on top of trends in the space via conferences, publications, social media, etc.
* Excellent spoken and written English
* You share our [values](/handbook/values), and work in accordance with those values.

## Nice-to-have

* Experience with remote-friendly, remote-mostly or remote-only work cultures
* Experience at multiple SaaS companies
* Experience within a start-up environment
* Direct experience in the Developer Tools or Collaboration spaces e.g. Atlassian, GitHub, JetBrains, Slack, Xamarin, New Relic, etc.
* Marketing experience beyond Product Marketing


**NOTE** In the compensation calculator below, fill in "Senior" in the `Level` field for this role.
