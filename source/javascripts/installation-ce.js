/* eslint-disable consistent-return */
var getUrlParameter = function getUrlParameter(param) {
  var pageURL = decodeURIComponent(window.location.search.substring(1));
  var urlParams = pageURL.split('&');

  for (var i = 0; i < urlParams.length; i++) {
    var parameterName = urlParams[i].split('=');

    if (parameterName[0] === param) {
      // eslint-disable-next-line no-undefined
      return parameterName[1] === undefined ? true : parameterName[1];
    }
  }
};

$(function() {
  // eslint-disable-next-line no-unused-vars
  var internalNavigationEvent = 'onpopstate' in window ? 'popstate' : 'hashchange';
  var distro = getUrlParameter('distro');

  if (distro) {
    document.getElementById('ce_link').href = '/installation/#' + distro + '?version=ce';
  }
});
