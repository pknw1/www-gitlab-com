---
layout: markdown_page
title: "Objectives and Key Results (OKRs)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What are OKRs?

OKRs are our quarterly goals to execute our [strategy](https://about.gitlab.com/strategy/). To make sure our goals are clearly defined and aligned throughout the organization. For more information see [Wikipedia](https://en.wikipedia.org/wiki/OKR) and [Google Drive](https://docs.google.com/presentation/d/1ZrI2bP-XKEWDWsT-FLq5piuIwl84w9cYH1tE3X5oSUY/edit) (GitLab internal). The OKRs are our quarterly goals

## Format

Before the quarter:

`Owner: Key Result as a sentence. Metric`

During and after the quarter:

`Owner: Key Result as a sentence. Metric => Result`

- Owner is the title of person that will own the result.
- We use two spaces to indent instead of tabs.
- OKRs start with the owner of the key result. When referring to a team lead we don't write 'lead' since it is shorter and the team goal is the same.
- The key result can link to an issue.
- The metric can link to real time data about the current state.
- The three company/CEO objectives are level 3 headers to provide some visual separation.

## Levels

We only list your key results, these have your (team) name on them.
Your objectives are the key results under which your key results are nested, these should normally be owned by your manager.
We do OKRs up to the team or director level, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/).
Part of the individual performance review is the answer to: how much did this person contribute to the team key objects?
We have no more than [five layers in our team structure](https://about.gitlab.com/team/structure/).
Because we go no further than the team level we end up with a maximum 4 layers of indentation on this page (this layer count excludes the three company/CEO objectives).
The match of one "nested" key result with the "parent" key result doesn't have to be perfect.
Every team should have at most 9 key results. To make counting easier only mention the team and people when they are the owner.
The advantage of this format is that the OKRs of the whole company will fit on 3 pages, making it much easier to have an overview.

## Updating

The key results are updated continually throughout the quarter when needed.
Everyone is welcome to a suggestion to improve them.
To update: make a merge request and assign it to the CEO.
If you're a [team member](https://about.gitlab.com/team/) or in the [core team](https://about.gitlab.com/core-team/) please post a link to the MR in the #okrs channel and at-mention the CEO.

At the top of the OKRs is a link to the state of the OKRs at the start of the quarter so people can see a diff.

Timeline of how we draft the OKRs:

1. Executive team pushes updates to this page: 4 weeks before the start of the quarter
1. Executive team 90 minute planning meeting: 3 weeks before the start of the quarter
1. Discuss with the board and the teams: 2 weeks before the start of the quarter
1. Executive team 'how to achieve' presentations: 1 week before the start of the quarter
1. Add Key Results to top of 1:1 agenda's: before the start of the quarter
1. Present OKRs at a functional group update: first week of the quarter
1. Present 'how to achieve' at a functional group update: during first three weeks of the quarter
1. Review previous quarter and next during board meeting: after the start of the quarter

## Critical acclaim

Spontaneous chat messages from team members after introducing this:

- As the worlds biggest OKR critic, This is such a step in the right direction :heart: 10 million thumbs up
- I like it too, especially the fact that it is in one page, and that it stops at the team level.
- I like: stopping at the team level, clear reporting structure that isn't weekly, limiting KRs to 9 per team vs 3 per team and 3 per each IC.
- I've been working on a satirical blog post called called "HOT NEW MANAGEMENT TREND ALERT: RJGs: Really Just Goals" and this is basically that. :wink: Most of these are currently just KPIs but I won't say that too loudly :wink: It also embodies my point from that OKR hit piece: "As team lead, it’s your job to know your team, to keep them accountable to you, and themselves, and to be accountable for your department to the greater company. Other departments shouldn’t care about how you measure internal success or work as a team, as long as the larger agreed upon KPIs are aligned and being met."
- I always felt like OKRs really force every person to limit freedom to prioritize and limit flexibility. These ones fix that!


## Current (2017-Q4)

### Objective 1: Grow incremental ACV according to plan.

* CRO: Sales efficiency ratio > 1.2 (IACV / sales+marketing)
  * CRO: Field Sales efficiency ratio > 1.8 (IACV / sales spend)
  * CMO: Marketing efficiency ratio > 3.4 (IACV / marketing spend)
* CMO: Establish credibility and thought leadership with Enterprise Buyers delivering on pipeline generation plan through the development and activation of integrated marketing and sales development campaigns:
  * CMO: Define category strategy, positioning and messaging and mobilize the company around the strategy.
    * CMO: Develop and document messaging framework
    * PMM: Develop and roll out updated pitch and analyst decks
    * PMM: Develop Action Plan for Q1/Q2 activation of the strategy
  * PMM: Develop GTM Strategy for EEU
    * Enable Sales with decks and Early Adopter Program     
  * CMO: Implement website redesign to support our awareness and lead generation objectives, accounting for distinct audiences.
  * MSD: achieve target in new inbound opportunity SQL $
  * MSD: achieve target in new outbound opportunity SQL $
  * MSD: achieve new opportunity volume target in strategic Sales segment accounts
  * CMO: Build out Product Marketing function, including hiring and on-boarding three people, updating objectives, process and handbook, and developing cross-functional alignment.
    * PMM: Evolve and deliver updated AE pitch deck messaging and incorporate Sales feedback
      * PMM: Update current “toolchain DevOps” to EE pitch Deck
      * PMM: Create CE to EE “get a demo” Deck
      * PMM: Create CE to EE Pitch Deck
      * PMM: Create SVN to EE pitch Deck
    * PMM: Develop a website Information Architecture (IA) to roll out in 2018
    * PMM: Enhance ROI section of website including adding interstitial to promote individual calculators
    * PMM: Work with content team to deliver 5 customer case studies or customer-centric blog posts.
    * PMM: Evolve EEP vs EES vs CE differentiation messaging and optimize website experience for product sections
* CRO: 100% of new business IACV plan
  * CRO: Sales Health 80% of Reps on Quota
  * CRO: [Increase Average Sales Assisted New Business Deal by 30%](https://na34.salesforce.com/00O61000003jNT6)
    *  RD: 70% of sales assisted deals are EEP
    *  CS: Launch Professional Services - 25% of all deals with subscription ACV of $100,000+ include PS
  * CRO: Decrease Time to close (New Business - Sales Assisted) by 10%
    * Sales Ops: Launch MEDDPIC. Fields 100% filled out for all stage 3+ deals
* CRO: 165% net retention in [Q4 renewal value](https://na34.salesforce.com/00O61000001uN0a)
  * Sales Ops: 95% of subscriptions under $1200 moved to auto-renew
  * CS: 25% of large/strategic accounts on EES trialling EEP with how we expand game plan documented in SFDC
  * CS: Double License Usage within our large and strategic accounts
  * CS: Identity the trigger(s) to purchase for large/strategic accounts
  * CS: Build Customer Advisory board program
* CFO: Efficiency
  * Legal: Publish sales training guide on key legal terms common in deals
  * Legal: Hold one sales team training session published on courses
  * Legal: Implement a legal section for sales team on-boarding
  * Legal: Use GitLab issues to track and collaborate legal matters
  * Controller: Plan for implementing ASC 606 approved
  * Controller: Move billing administration from SMB team to billing specialist
  * Billing Specialist: Create a Zuora training module for quick and efficient training of new sales reps.
  * Analytics: Create a user journey funnel

### Objective 2: Popular next generation product.

* CEO: Next generation
  * VP Product
    * Platform: Make way for a cloud IDE. Multi-file editor with terminal shipped.
    * Discussion: Portfolio management: Epics and roadmap view shipped (as part of EEU)
    * CI/CD: Improve support for Java development lifecycle. 1 more project done.
    * CI/CD: Improve existing features with additional value. 1 feature extended.
    * Prometheus: Make GitLab easier to monitor. [GA Prometheus monitoring of the GitLab server](https://gitlab.com/gitlab-org/gitlab-ce/issues/38568), deprecate InfluxDB.
    * Prometheus: Shift performance monitoring earlier in the pipeline, [detecting regressions prior to merge](https://gitlab.com/gitlab-org/gitlab-ee/issues/3173). Deliver one feature.
    * Prometheus: Complete the feedback loop by [comparing Canary and Stable performance](https://gitlab.com/gitlab-org/gitlab-ee/issues/2594).
  * VP Eng
    * Partner with PM to generate and implement process to measure and speed up development velocity
  * Support Lead
    * 85% Premium Support SLA (up from 68% last quarter)
    * +5% MoM on all other SLAs
    * Identify and close 100 tickets of debt in backlog
  * Design Lead
    * [Document Gitlab UX standards / style guide and communicate  to internal team via UX FGU, the UX Style guide, and within issues](https://gitlab.com/gitlab-org/gitlab-design/issues/67)
    * [Finish Phase 1 of a design library of assets](https://gitlab.com/gitlab-org/gitlab-design/issues/26)
  * Frontend (AC) Lead
    * Write 60 unit tests to resolve test debt in Q4 (evenly distributed throughout the team)
    * Crush 140 bugs this quarter (evenly distributed through the team)
    * Improve codebase by making modules ready for webpack by moving it to our new coding standards ([#38869](https://gitlab.com/gitlab-org/gitlab-ce/issues/38869))
    * Improve performance by making library updates and webpack bundle optimizations ([#39072](https://gitlab.com/gitlab-org/gitlab-ce/issues/39072))
    * Finish conversion from inline icons to SVG Icons to improve performance
  * Frontend (DC) Lead
    * Write 60 unit tests to resolve test debt in Q4 (evenly distributed throughout the team)
    * Crush 140 bugs this quarter (evenly distributed through the team)
    * Refactor the MR discussion in Vue to decrease load times, and increase performance/usability
    * Remove global namespaces, to enable webpack code splitting, which improves performance ([#38869](https://gitlab.com/gitlab-org/gitlab-ce/issues/38869))
  * Director of Backend
    * Author a demo script for use throughout the quarter
    * Expose EE and CE code coverage metrics
  * Build Lead
    * Establish baseline metric for install time/ease and come up with a plan to achieve and maintain it
    * Decrease build times from 60 minutes to 30 minutes
    * Create integration test for Mattermost
  * Platform Lead
    * Identify 1 sub-standard area of the code base and raise local unit test coverage up to project level
    * Write integration test for backup/restore
    * Make GitLab QA [test LDAP](https://gitlab.com/gitlab-org/gitlab-qa/issues/3)
    * Resolve or schedule all priority 1 & 2 Platform issues (and groom performance issues)
  * CI/CD Lead
    * Add 1 integration for runners
    * Resolve or schedule all priority 1 & 2 CI/CD issues (and groom performance issues)
    * Reduce amount of system failures to less than 0.1%
    * Improve cost efficiency of CI jobs processing for GitLab.com and GitLab Inc.
  * Discussion Lead
    * Write integration test for squash/rebase
    * Write integration test for protected branches
    * Resolve or schedule all Priority 1 & 2 Discussion issues (and groom performance issues)
  * Prometheus Lead
    * Reach parity with Prometheus metrics for Unicorn, Sidekiq, and gitlab-shell and Deprecate InfluxDB
    * Make Grafana dashboards available for all Prometheus data easy to install for GitLab instances
    * Identify 1 sub-standard area of the code base and raise local unit test coverage up to project level
    * Resolve or schedule all Priority 1 & 2 Prometheus issues (and groom performance issues)
  * Geo Team
    * Make Geo Generally Available
    * Geo performant at GitLab.com scale
    * Manual failover robust in Geo as first step to Disaster Recovery
  * Director of Quality
    * Document what quality means at GitLab on an about page
    * Communicate standard in 3 different ways to internal team
    * Make issue/board scheme change recommendation to allow us to better mine backlog for quality metrics
    * Initiate a project to make quality metrics and charts self-service
    * Initiate a project to allow for UI testing of the web application locally and on CI
  * Edge Lead
    * Ship [large database seeder for developers](https://gitlab.com/gitlab-org/gitlab-ce/issues/28149)
    * Enable [triage](https://gitlab.com/gitlab-org/triage) to be [used for any project](https://gitlab.com/gitlab-org/triage/issues/14#note_41293856)
    * Make GitLab QA [test the Container Registry](https://gitlab.com/gitlab-org/gitlab-qa/issues/49)
    * Make GitLab QA [test upgrade from CE to EE](https://gitlab.com/gitlab-org/gitlab-qa/issues/64)
    * Make GitLab QA [test simple push with PostReceive](https://gitlab.com/gitlab-org/gitlab-qa/issues/53)
    * [De-duplicate at least 5 redundant (feature) tests](https://gitlab.com/gitlab-org/gitlab-ce/issues/39829)
    * Improve at least the [5 longest spec files](https://redash.gitlab.com/queries/15) by at least 30%
    * Investigate code with less than 60% tests coverage and add tests for  at least the [5 most critical files](https://gitlab.com/gitlab-org/gitlab-ce/issues/19412)
    * [Investigate encapsulating instance variables](https://gitlab.com/gitlab-org/gitlab-ce/issues/20045) about the current page in a class
    * [Reduce duplication](https://gitlab.com/gitlab-org/gitlab-ce/issues/31574) in at least 5 forms
    * Solve at least 3 [outstanding performance issues](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=performance&milestone_title=No+Milestone)
  * Director of Security
    * Security Product. Document product vision and roadmap to MVP.
    * Compliance framework. Plan and first implementation.
  * CTO: [Scan source code for security issues](https://gitlab.com/gitlab-org/gitlab-ce/issues/38413 ). Make it work for 3 popular frameworks.
  * CTO: [Less effort to merge CE into EE](https://gitlab.com/gitlab-org/gitlab-ee/issues/2952#note_41016654). 10 times less efforts to merge CE to EE
  * CTO: Start new projects that might materially affect the scope and future of the company.
* CEO: Partnerships
  * VP Product
    * Increase adoption of Kubernetes through integration.
    * CI/CD: Configure review apps and deployment for projects in less than 5 steps.
    * Prometheus: Enable [Prometheus monitoring of Kubernetes clusters]((https://gitlab.com/gitlab-org/gitlab-ce/issues/28916)) with a single click.
    * Platform: Help partners and customers adopt GitLab. Ship authentication and integration requirements.
    * Platform: Ship the GNOME requirements. 5 requirements shipped.
    * At least 3 GNOME projects migrated to GitLab as part of evaluation
    * [AWS QuickStart guide](https://gitlab.com/gitlab-org/gitlab-ce/issues/29199) published
* CEO: Preparing GitLab.com to be mission-critical
  * VP Product
    * Improve GitLab.com subscriptions. Storage size increase per subscription level. Ability to upgrade easily.
  * VP Eng
    * Generate and implement plan to align product development with simultaneous CE, EE, and gitlab.com needs
  * Build Lead
    * Build GCP deployment mechanism on Kubernetes for the migration
  * Platform Lead
    * Finish [Circuit breakers](https://gitlab.com/gitlab-org/gitlab-ce/issues/37383)
  * Director of Infrastructure
    * Generate a project plan for the GCP migration and get approved by EJ and Sid
    * Execute milestone 0 of the GCP migration plan by TBD
    * Execute milestone 1 of the GCP migration plan by TBD
  * Gitaly Lead
    * 100% of Git operations on Gitlab.com go through Gitaly (Gitaly v1.0)
    * Demo Gitaly fast-failure during a file-server outage
  * Database Lead
    * Demo restore time < 1 hour
    * Solve 30% of the schema issues identified by Crunchy
    * Database Uptime 99.99% measured in Prometheus
    * SQL timing under 100ms for Issue, MR, project dashboard, and CI pages measured in Prometheus
  * Director of Security
    * Strong security for SaaS and on-premise product. Top 10 actions from risk assessment done and actions for top 10 risks started.
    * HackerOne bug bounty program. Implemented and bounties awarded.
    * Security policies for cloud services and cloud migrations. Policy published and enacted.
* CMO: Build trust of, and preference for GitLab among software developers
  * CMO: Hire Director, DevRel/Community Relations
    * CMO: Build out DevRel/Community Relations function
    * CA: Grow community and increase community engagement. Increase number of new contributors by 10%, increase number of total contributions per release by 5% and increase number of Twitter mentions of GitLab by 10%.
  * PMM: Support field marketing at AWS: Reinvent & KubeCon with booth decks and training  
  * MSD: $600K in self serve revenue.
  * MSD: Grow followers by 20% through proactive sharing of useful and interesting information across our social channels.
  * MSD: Grow number of opt-in subscribers to our newsletter by 20%.
* CMO: Generate more company and product awareness including increasing lead over [BitBucket in Google Trends](https://trends.google.com/trends/explore?q=bitbucket,gitlab)
  * MSD: Implement SEO/PPC program to drive increase in number of free trials by 20% compared to last quarter, increase number of contact sales requests by 22% compared to last quarter, increase amount of traffic to about.gitlab.com by 9% compared to last quarter
  * CMO: PR - October Announcements - 10.0, Series C, Wave, CLA
  * CMO: AR - v10 briefing sweep for targeted analysts

### Objective 3: Great team.

* CFO: Improve team productivity
  * Analytics: Data and Analytics vision and plan signed off by executive team
  * Analytics: Real time analytics implemented providing visual representation of the metrics sheet
  * Legal: Create plan for implementing Global Data Protection and Data Privacy Plan
  * Controller: Reduce time to close from 10 days to 9 days.
  * Accounting Manager: Identify and add to the handbook two new accounting policies.
  * Accounting Manager: Create monthly process for BvsA analysis with department budget owners.
* CCO: Create an Exceptional Corporate Culture / Delight Our Employees
  * Launch training for making employment decisions based on the GitLab Values. Launch by November 15th
  * Launch a short, quarterly Employee Pulse Survey. Strive for 80% completion
  * Analyze and make recommendations based off of New Hire Survey and Pulse surveys which will drive future KRs. Have at least 3 areas to improve each quarter. Ideally, we will also have 3 areas to celebrate.
  * Revise the format of the Morning Team Calls to allow for better participation and sharing. Strive for 80% participation
  * Improve use of the GitLab Incentives by 15%. https://about.gitlab.com/handbook/incentives/
  * Iterate on the Performance Review process with at least two changes initiated by end of year.
* CCO: Grow Our Team With A-Players
  * KR: Socialize and grow participation in our Diversity Referral bonuses by 10% (measurement should be made in January as many hires in December don’t start until January, with awareness that the actual bonuses aren’t paid out for 3 months)
  * More sourced recruiting. 20% of total hires
  * Ensure candidates are being interviewed for a fit to our Values as well as ability to do the job, through Manager Training and Follow-up by People Ops.
  * Hire Recruiting Director
  * 90% of all candidates will be advanced through the pipeline within 7 business days in each phase, maximum.
* CCO: Make All of Our Managers More Effective and Successful
  * Provide consistent training to managers on how to manage effectively. Success will mean that there are at least 15 live trainings a year in addition to curated online trainings.
  * Ensure every manager is doing regular 1-on-1 meetings with 2-way feedback. Measure will be seen in Employee Pulse survey, with at least 90% of employees indicating they have received feedback from their manager in the last month.
  * Hire People Business Partners to partner with managers to operate as leadership coaches, performance management advisors, talent scouts, and Culture/Values evangelists. Goal of 2 hires.
* VPE: Build the best, global, diverse engineering, design, and support teams in the developer platform industry
  * Revise hiring plan for Q1 2018 based on Q4 financials and product ambitions
  * Launch 2018 Q1 department OKRs before EOQ4 2017
  * Hire an additional Director of Engineering
* Support: Grow the support team to better comply with SLAs and cover gitlab.com cases
  * Hire a services support lead
  * Hire an support specialist
  * Hire an EMEA support engineer
  * Hire an EMEA/AMER support engineer
  * Hire an AMER support engineer
* UX: Increase the profile of GitLab design and grow the team
  * [Launch first iteration of design.gitlab.com](https://gitlab.com/gitlab-org/gitlab-design/issues/44)
  * [Write 3 public blog posts about GitLab UX and visual design case studies, best practices, anecdotes, or events](https://gitlab.com/gitlab-org/gitlab-design/issues/75)
  * Hire 2 UX designers
  * Hire a junior UX researcher
* Frontend (DC): Grow the team
  * Hire 3 front end developers
* Frontend (AC): Grow the team
  * Hire 2 front end developers
* Director of Backend: Grow the team
  * Hire a lead for geo
* Build: Grow the team
  * Hire 2 developers
  * Hire a senior developer
* CI/CD: Grow the team
  * Hire 3 developers
  * Hire 2 senior developers
* Discussion: Grow the team
  * Hire 2 developers
* Director of Infrastructure: Grow the team
  * Hire 2 DevOps engineers
  * Hire a database lead
* Gitaly: Grow the team
  * Hire a developer
* Database: Grow the team
  * Hire a database specialist
* Director of Quality: Grow the team
  * Hire a test automation lead
  * Hire 3 test automation engineers
* Director of Security: Build strong team.
  * Hire Security Engineer(s)
  * Hire a Security Specialist Developer

## Archive

* [2017 Q3](/okrs/2017-q3/)
